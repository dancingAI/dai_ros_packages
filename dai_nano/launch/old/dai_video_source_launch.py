import os
from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch_ros.actions import Node

import yaml

def generate_launch_description():

    config = os.path.join(get_package_share_directory('ros_dai'),'config','dai_config.yaml')

    return LaunchDescription([

        Node(
            package='ros_dai',
            node_executable='video_source',
            node_namespace='camera0',
            name='DaiVideoSource',
            parameters=[config],
            remappings=[
                ("raw", "/camera0/raw")
            ]
        ),

        Node(
            package='ros_dai',
            node_executable='video_source',
            node_namespace='camera1',
            name='DaiVideoSource',
            parameters=[config],
            remappings=[
                ("raw", "/camera1/raw")
            ]
        )

    ])

