import os
from launch import LaunchDescription
from launch_ros.actions import Node
# from launch.actions import IncludeLaunchDescription
# from launch.launch_description_sources import PythonLaunchDescriptionSource
# from launch.substitutions import ThisLaunchFileDir
from ament_index_python.packages import get_package_share_directory

# import yaml

def generate_launch_description():

    config = os.path.join(get_package_share_directory('ros_dai'),'config','dai_config.yaml')

    return LaunchDescription([

        # launch source video nodes
        # IncludeLaunchDescription(
        #     PythonLaunchDescriptionSource([ThisLaunchFileDir(), '/dai_video_source_launch.py'])
        # ),
        Node(
            package='ros_dai',
            node_executable='video_source',
            node_namespace='camera0',
            name='DaiVideoSource',
            output='screen',
            emulate_tty=True,
            parameters=[config],
            remappings=[
                ("raw", "/camera0/raw")
            ]
        ),

        # Node(
        #     package='ros_dai',
        #     node_executable='video_source',
        #     node_namespace='camera1',
        #     name='DaiVideoSource',
        #     output='screen',
        #     emulate_tty=True,
        #     parameters=[config],
        #     remappings=[
        #         ("raw", "/camera1/raw")
        #     ]
        # ),

        # launch detectnet node
        # Node(
        #     package='ros_dai',
        #     node_executable='dai_node_detectnet',
        #     name='detectnet',
        #     remappings=[
        #         ("/detectnet/image_in_0", "/video_source/raw")
        #         ("/detectnet/image_in_1", "/video_source/raw")
        #     ],
        #     parameters=[config]
        # ),

        # launch output video nodes
        # IncludeLaunchDescription(
        #     PythonLaunchDescriptionSource([ThisLaunchFileDir(), '/dai_video_output_launch.py'])
        # ),

        Node(
            package='ros_dai',
            node_executable='video_render',
            node_namespace='camera0',
            name='DaiVideoRender',
            output='screen',
            emulate_tty=True,
            parameters=[config]
        ),

        # Node(
        #     package='ros_dai',
        #     node_executable='video_render',
        #     node_namespace='camera1',
        #     name='DaiVideoRender',
        #     output='screen',
        #     emulate_tty=True,
        #     parameters=[config]
        # )

    ])