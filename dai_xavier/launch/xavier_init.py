"""
    Launches all Jetson Xavier sensor nodes: Pozyx
"""
import socket
from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():
    return LaunchDescription([

        # pozyx ---------------------------- #
        Node(
            package='dai_xavier',
            node_namespace='/'+socket.gethostname(),
            node_executable='node_pozyx.py',
            remappings=[
                ('/'+socket.gethostname()+'/rosout', '/rosout'),
                ('/'+socket.gethostname()+'/parameter_events', '/parameter_events')
            ]
        ),

    ])
