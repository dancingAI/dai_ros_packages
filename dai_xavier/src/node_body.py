# ------------------------------------------------------------------------------ #
# 
# DaiBody Node : connects all sensors and actuators of Dai (using ROS) and
#                compute rewards.
#  
# ------------------------------------------------------------------------------ #

from collections import deque
import numpy as np
import time
import itertools

import rclpy
from rclpy.node import Node

from ControlInput import ControlInput
from ControlOutput import ControlOutput

# ROS messages
from dai_main.msg import WheelTargets as WheelTargetsMsg
from dai_main.msg import SliderTargets as SliderTargetsMsg
from dai_main.msg import SliderPosition as SliderPositionMsg
from dai_main.msg import EulerAngles as EulerAnglessMsg
from dai_main.msg import PozyxData as PozyxDataMsg

class DaiBodyNode(Node):
    def __init__(self, performance, safety_module):
        super().__init__('daiBody_node')

        self._slider_pos = np.zeros(3)
        self._pozyx_position = np.zeros(3)
        self._pozyx_quaternion = np.zeros(4)
        self._pozyx_angles_rpy = np.zeros(3)

        self._slider_targets = SliderTargetsMsg()
        self._wheels_targets = WheelTargetsMsg()

        self._performance = performance
        self._safety_module = safety_module

        self._experience_memory = deque()

        # reward variables
        default_speed = 0.1
        self._circle_diameter = 4.0
        circle_circumference = self._circle_diameter * np.pi
        self._lap_time = circle_circumference / default_speed

        self._slider1_reward = False
        self._slider2_reward = False
        self._slider3_reward = False

        self._yaw_reward = False
        self._pitch_reward = False
        self._roll_reward = False

        # subscribe to slider position, pozyx, sonars
        self._slider1_pos_sub = self.create_subscription(SliderPositionMsg, '/sensors/slider1_pos', self._update_slider_pos, 10)
        self._slider1_pos_sub
        self._slider2_pos_sub = self.create_subscription(SliderPositionMsg, '/sensors/slider2_pos', self._update_slider_pos, 10)
        self._slider2_pos_sub
        self._slider3_pos_sub = self.create_subscription(SliderPositionMsg, '/sensors/slider3_pos', self._update_slider_pos, 10)
        self._slider3_pos_sub
        self._pozyx_sub = self.create_subscription(PozyxDataMsg, '/sensors/pozyx_pose', self._update_pose, 10)
        self._pozyx_sub
        # self.sonars_sub = self.create_subscription(Pose, '/sensors/sonars', self._update_sonars, 10)
        # self.sonars_sub

        # create publishers for wheel and slider targets
        self._wheels_target_pub = self.create_publisher(WheelTargetsMsg, '/targets/wheels', 10)
        self._sliders_target_pub = self.create_publisher(SliderTargetsMsg, '/targets/sliders', 10)

        self.get_logger().info("INIT_SUCCESS") # for user command service


    # private methods -------------------------------------------------------------------------------------------------- #

    def _update_slider_pos(self, msg):
        slider_id = msg.slider_id
        self._slider_pos[int(slider_id)-1]: msg.slider_position

    def _update_pose(self, msg):
        self._pozyx_position   = np.array([ msg.position.x,
                                           msg.position.y,
                                           msg.position.z ])
        self._pozyx_quaternion = np.array([ msg.orientation.x,
                                           msg.orientation.y,
                                           msg.orientation.z,
                                           msg.orientation.w ])
        self._pozyx_angles_rpy = np.array([ msg.angles.roll,
                                           msg.angles.pitch,
                                           msg.angles.yaw ])

    def _update_sonars(self, msg):
        pass

    def _update_averages(self, rem_input: ControlInput, new_input: ControlInput, count:int):
        self.average_wheel1a_speed += (new_input.Wheel1aTargetSpeed - rem_input.Wheel1aTargetSpeed - self.average_wheel1a_speed) / count
        self.average_wheel1b_speed += (new_input.Wheel1bTargetSpeed - rem_input.Wheel1bTargetSpeed - self.average_wheel1b_speed) / count
        self.average_wheel2a_speed += (new_input.Wheel2aTargetSpeed - rem_input.Wheel2aTargetSpeed - self.average_wheel2a_speed) / count
        self.average_wheel2b_speed += (new_input.Wheel2bTargetSpeed - rem_input.Wheel2bTargetSpeed - self.average_wheel2b_speed) / count
        self.average_wheel3a_speed += (new_input.Wheel3aTargetSpeed - rem_input.Wheel3aTargetSpeed - self.average_wheel3a_speed) / count
        self.average_wheel3b_speed += (new_input.Wheel3bTargetSpeed - rem_input.Wheel3bTargetSpeed - self.average_wheel3b_speed) / count
        self.average_slider1_speed += (new_input.Slider1TargetSpeed - rem_input.Slider1TargetSpeed - self.average_slider1_speed) / count
        self.average_slider2_speed += (new_input.Slider2TargetSpeed - rem_input.Slider2TargetSpeed - self.average_slider3_speed) / count
        self.average_slider3_speed += (new_input.Slider3TargetSpeed - rem_input.Slider3TargetSpeed - self.average_slider3_speed) / count
        self.average_slider1_position += (new_input.Slider1Position - rem_input.Slider1Position - self.average_slider1_position) / count
        self.average_slider2_position += (new_input.Slider2Position - rem_input.Slider2Position - self.average_slider2_position) / count
        self.average_slider3_position += (new_input.Slider3Position - rem_input.Slider3Position - self.average_slider3_position) / count
        self.average_yaw   += (new_input.YawAngle   - rem_input.YawAngle   - self.average_yaw)   / count
        self.average_pitch += (new_input.PitchAngle - rem_input.PitchAngle - self.average_pitch) / count
        self.average_roll  += (new_input.RollAngle  - rem_input.RollAngle  - self.average_roll)  / count
        self.average_x += (new_input.XPos - rem_input.XPos - self.average_x) / count
        self.average_y += (new_input.YPos - rem_input.YPos - self.average_y) / count
        
        # self.locations_visited[int(rem_input.XPos)][int(rem_input.YPos)] -= 1
        location_x = new_input.XPos + self._performance.xLength/2
        location_x = location_x if location_x >= 0 else 0
        location_x = location_x if location_x <= self._performance.xLength - 1 else self._performance.xLength - 1
        location_y = new_input.YPos + self._performance.yLength/2
        location_y = location_y if location_y >= 0 else 0
        location_y = location_y if location_y <= self._performance.yLength - 1 else self._performance.yLength - 1
        self.locations_visited[int(location_x)][int(location_y)] = 1
        
        self.get_logger().info("Location explored: "+str(location_x)+"/"+str(location_y))

    def _get_reference_position(self, lapPercentage):
        radians = 2 * np.pi * lapPercentage
        return Coordinate(np.sin(radians) * self._circle_diameter / 2, np.cos(radians) * self._circle_diameter / 2)

    def _get_error(self, position, expectedPosition):
        return Coordinate(expectedPosition.x - position.x, expectedPosition.y - position.y)

    def _get_slider_reward(self, elapsed_time)->(float, bool):
            count = len(self._experience_memory)
            if count > 20 :
                old_batch = list(itertools.islice(self._experience_memory, count-20, count-11))
                new_batch = list(itertools.islice(self._experience_memory, count-10, count-1))
                event = self._experience_memory[count-1]
                # old_slider1 = np.average(np.asarray([e.Slider1TargetSpeed for e in old_batch]))
                # new_slider1 = np.average(np.asarray([e.Slider1TargetSpeed for e in new_batch]))
                # old_slider2 = np.average(np.asarray([e.Slider2TargetSpeed for e in old_batch]))
                # new_slider2 = np.average(np.asarray([e.Slider2TargetSpeed for e in new_batch]))
                # old_slider3 = np.average(np.asarray([e.Slider3TargetSpeed for e in old_batch]))
                # new_slider3 = np.average(np.asarray([e.Slider3TargetSpeed for e in new_batch]))
                old_slider1 = np.average(np.asarray([e.Slider1Position for e in old_batch]))
                new_slider1 = np.average(np.asarray([e.Slider1Position for e in new_batch]))
                old_slider2 = np.average(np.asarray([e.Slider2Position for e in old_batch]))
                new_slider2 = np.average(np.asarray([e.Slider2Position for e in new_batch]))
                old_slider3 = np.average(np.asarray([e.Slider3Position for e in old_batch]))
                new_slider3 = np.average(np.asarray([e.Slider3Position for e in new_batch]))
                
                self.get_logger().info('Slider pos: {} {} {}'.format(event.Slider1Position, event.Slider2Position, event.Slider3Position))
                self.get_logger().info('Slider 1 old / new: {} / {}'.format(old_slider1, new_slider1))
                self.get_logger().info('Slider 2 old / new: {} / {}'.format(old_slider2, new_slider2))
                self.get_logger().info('Slider 3 old / new: {} / {}'.format(old_slider3, new_slider3))

                s1_reward = 0
                s2_reward = 0
                s3_reward = 0

                # Reward should vary between 0 and +1 per slider (so 0 to 3 total)
                # Reward is reduced to 0.2 until or sliders are moving
                # Reward is -1 when end reached

                if not self._slider1_reward:
                    s1_reward = (np.absolute(old_slider1 - new_slider1) / 50)**2
                    if s1_reward >= 10000:
                        self._slider1_reward = True
                else:
                    s1_reward = np.absolute(old_slider1 - new_slider1) / (50*25)
                s1_reward = s1_reward if event.Slider1Position < 300 and event.Slider1Position > -300 else -1

                if not self._slider2_reward:
                    s2_reward = (np.absolute(old_slider2 - new_slider2) / 50)**2
                    if s2_reward >= 10000:
                        self._slider2_reward = True
                else:
                    s2_reward = np.absolute(old_slider2 - new_slider2) /  (50*25)
                s2_reward = s2_reward if event.Slider2Position < 300 and event.Slider2Position > -300 else -1
    
                if not self._slider3_reward:
                    s3_reward = (np.absolute(old_slider3 - new_slider3) / 50)**2
                    if s3_reward >= 10000:
                        self._slider3_reward = True
                else:
                    s3_reward = np.absolute(old_slider3 - new_slider3) /  (50*25)
                s3_reward = s3_reward if event.Slider3Position < 300 and event.Slider3Position > -300 else -1
                
                if self._slider1_reward and self._slider2_reward and self._slider3_reward:
                        self._slider1_reward = False
                        self._slider2_reward = False
                        self._slider3_reward = False
                s_reward = s1_reward + s2_reward + s3_reward
                self.get_logger().info('Slider reward: {} + {} + {} = {}'.format(s1_reward,s2_reward,s3_reward,s_reward))
                return s_reward, False

            else :
                return self._get_slider_reward_simple(elapsed_time)

    def _get_slider_reward_simple(self, elapsed_time: float) -> (float, bool):
        # Reward should vary between 0 and +1 per slider (so 0 to 3 total)
        # Reward is -1 when end reached
        event = self._experience_memory[len(self._experience_memory)-1]
        reward = np.absolute(event.Slider1TargetSpeed) / 30 if event.Slider1Position < 300 and event.Slider1Position > -300 else -1
        reward += np.absolute(event.Slider2TargetSpeed) / 30 if event.Slider2Position < 300 and event.Slider2Position > -300 else -1
        reward += np.absolute(event.Slider3TargetSpeed) / 30 if event.Slider3Position < 300 and event.Slider3Position > -300 else -1
        
        self.get_logger().info('Slider reward: {}'.format(reward))
        self.get_logger().info('Positions: {} {} {}'.format(event.Slider1Position, event.Slider2Position, event.Slider3Position))
        
        return reward, False

    def _get_location_reward(self, elapsed_time: float) -> (float, bool):
        # Reward should vary between -1 and +1
        percentage_of_time = elapsed_time / self._performance.duration
        target_visits = int(self._performance.duration / 4 * percentage_of_time) #increase the divider to reduce the number of target locations
        target_visits = target_visits if target_visits > 0 else 1.0
        visit_count = sum(map(sum, self.locations_visited))
        reward = 2*(1 + (target_visits - visit_count)/target_visits) if visit_count > target_visits else 2*(1 + (visit_count - target_visits)/target_visits)

        self.get_logger().info('Location reward: {} Target: {}  Visited: {}'.format(reward, target_visits, visit_count))
        
        return reward, False

    def _get_angular_reward(self, elapsed_time) -> (float, bool):
        count = len(self._experience_memory)
        event = self._experience_memory[count-1]
        self.get_logger().info('Angular pos: {} {} {}'.format(event.YawAngle, event.PitchAngle, event.RollAngle))

        # s1_reward = 0 if event.Slider1Position < 290 and event.Slider1Position > -290 else -1
        # s2_reward = 0 if event.Slider2Position < 290 and event.Slider2Position > -290 else -1
        # s3_reward = 0 if event.Slider3Position < 290 and event.Slider3Position > -290 else -1

        if count > 20 :
            old_batch = list(itertools.islice(self._experience_memory, count-20, count-11))
            new_batch = list(itertools.islice(self._experience_memory, count-10, count-1))
            old_yaw = np.average(np.asarray([e.YawAngle for e in old_batch]))
            new_yaw = np.average(np.asarray([e.YawAngle for e in new_batch]))
            old_pitch = np.average(np.asarray([e.PitchAngle for e in old_batch]))
            new_pitch = np.average(np.asarray([e.PitchAngle for e in new_batch]))
            old_roll = np.average(np.asarray([e.RollAngle for e in old_batch]))
            new_roll = np.average(np.asarray([e.RollAngle for e in new_batch]))

            # Reward should vary between 0 and 4
            # Reward is reduced to 0.8 until all angles are rotated
            
            y_reward = 0
            p_reward = 0
            r_reward = 0

            if not self._yaw_reward:
                y_reward =  np.absolute(old_yaw - new_yaw) / 45
                if y_reward >= 1:
                    self._yaw_reward = False # True
            else:
                y_reward = np.absolute(old_yaw - new_yaw) / (45 * 5)

            if not self._pitch_reward:
                p_reward =  np.absolute(old_pitch - new_pitch) / 45
                if p_reward >= 1:
                    self._pitch_reward = False # True
            else:
                p_reward = np.absolute(old_pitch - new_pitch) / (45 * 5)

            if not self._roll_reward:
                r_reward =  np.absolute(old_roll - new_roll) / 45
                if r_reward >= 1:
                    self._roll_reward = False # True
            else:
                r_reward = np.absolute(old_roll - new_roll) / (45 * 5)
            
            if self._yaw_reward and self._pitch_reward and self._roll_reward:
                    self._yaw_reward = False
                    self._pitch_reward = False
                    self._roll_reward = False

            angular_reward = y_reward + p_reward + r_reward

            self.get_logger().info('Angular reward Yaw + Pitch + Roll = Total: {} + {} + {} = {}'.format(y_reward, p_reward, r_reward, angular_reward))
            self.get_logger().info('Angular reward Old/New: Yaw: {} / {} Pitch: {} / {} Roll: {} / {}'.format(old_yaw, new_yaw, old_pitch, new_pitch, old_roll, new_roll))

            return angular_reward, False # + s1_reward + s2_reward + s3_reward, False

        else :
            return self._get_circular_reward(elapsed_time)[0], False # + s1_reward + s2_reward + s3_reward, False
            # return s1_reward + s2_reward + s3_reward, False

    def _get_circular_reward(self, elapsed_time: float) -> (float, bool):
        # Reward is a maximum of 2 and a minimum of -2
        state = self.observe_control_inputs()
        laps = elapsed_time / self._lap_time
        self.expected_position = self._get_reference_position(laps - np.trunc(laps))
        error = self._get_error(Coordinate(state.XPos, state.YPos), self.expected_position).get_distance()
        
        self.get_logger().info('Expected: {}, {} / Current: {}, {}'.format(self.expected_position.x, self.expected_position.y, state.XPos, state.YPos))

        reward = (self._circle_diameter/2 - error)
        run_terminate = False
        if error > self._circle_diameter:
            run_terminate = True
            self.get_logger().error("Terminating on error in _get_circular_reward")
        return reward, run_terminate

    def _get_energy_management_reward(self, elapsed_time: float) -> (float, bool):
        percentage_of_time = elapsed_time / self._performance.duration
        event = self._experience_memory[len(self._experience_memory)-1]

        # Accumulate energy by default
        self.remaining_energy += 6 #10

        # Full power on 3 wheels depletes energy reserve in 5s, 20 units per iteration
        wheelEnergy = (
            np.absolute(event.Wheel1aTargetSpeed) + np.absolute(event.Wheel2aTargetSpeed) + np.absolute(event.Wheel3aTargetSpeed) +
            np.absolute(event.Wheel1bTargetSpeed) + np.absolute(event.Wheel2bTargetSpeed) + np.absolute(event.Wheel3bTargetSpeed)) / 6 #1.5

        # Full power on 3 sliders depletes energy reserve in 6.5s, 15 units per iteration
        sliderEnergy = (np.absolute(event.Slider1TargetSpeed) + np.absolute(event.Slider2TargetSpeed) + np.absolute(event.Slider3TargetSpeed)) / 120 #24 #6

        self.remaining_energy -= (wheelEnergy + sliderEnergy)

        reward = 0
        # negative reward if not doing enough
        if self.remaining_energy > 100:
            reward = (100 - self.remaining_energy) / 50
        # negative reward if doing too much
        elif self.remaining_energy < -100:
            reward = (100 + self.remaining_energy) / 50
        # reward for taking the time to recuperate
        elif self.remaining_energy > 50:
            reward = 2 * (100 - self.remaining_energy) / 50
        # reward for using bursts of energy
        elif self.remaining_energy < -50:
            reward = 2 * (100 + self.remaining_energy) / 50

        self.get_logger().info('Energy management reward: {} Wheel energy: {} Slider energy: {} Remaining energy: {}'.format(reward, wheelEnergy, sliderEnergy, self.remaining_energy))
        
        return reward, False

    def _get_energy_conservation(self, elapsed_time: float) -> (float, bool):
        """ NOT USED """
        # Reward should vary between -3 and +1.5
        # Missing absolute values!!!
        event = self._experience_memory[len(self._experience_memory)-1]
        reward = (40 - (event.Slider1TargetSpeed/3 + event.Slider2TargetSpeed/3 + event.Slider3TargetSpeed/3 +
            event.Wheel1aTargetSpeed*10 + event.Wheel2aTargetSpeed*10 + event.Wheel3aTargetSpeed*10 +
            event.Wheel1bTargetSpeed*10 + event.Wheel2bTargetSpeed*10 + event.Wheel3bTargetSpeed*10))
        reward = reward / 20
        self.get_logger().info('Energy reward: {}'.format(reward))
        return reward, False

    def _get_geometric_reward(self, elapsed_time: float) -> (float, bool):
        """ NOT USED """
        reward = 0
        if (len(self._experience_memory) >= 2):
            now = self._experience_memory[len(self._experience_memory)-1]
            before = self._experience_memory[len(self._experience_memory)-2]
            xn = now.XPos
            yn = now.YPos
            xb = before.XPos
            yb = before.YPos
            
            # Take a 1m safety margin
            xL = self._performance.xLength - 2 * self._safety_module._safety_margin - 1
            yL = self._performance.yLength - 2 * self._safety_module._safety_margin - 1

            # Nice big circle radius
            R = xL/2.0 if xL < yL else yL/2.0
            R2 = R**2

            # Distances from center squared
            dn2 = xn**2 + yn**2
            db2 = xb**2 + yb**2
            # Distance between the 2 points
            dnb2 = (xn-xb)**2 + (yn-yb)**2

            
            # if a reasonable amount of movement and both points are close to the radius give a reward
            if dnb2 > 0.1*R2 and (dn2 < 1.10*R2 and dn2 > 0.90*R2) and (db2 < 1.10*R2 and db2 > 0.90*R2):
                reward = dnb2/(0.1*R2)

            self.get_logger().info('Geometric reward: {} for diameter: {}'.format(reward, R))
        return reward, False

    # public methods --------------------------------------------------------------------------------------------------- #

    def reset(self):
        self._experience_memory = deque()

        self.average_wheel1a_speed = 0
        self.average_wheel1b_speed = 0
        self.average_wheel2a_speed = 0
        self.average_wheel2b_speed = 0
        self.average_wheel3a_speed = 0
        self.average_wheel3b_speed = 0
        self.average_slider1_speed = 0
        self.average_slider2_speed = 0
        self.average_slider3_speed = 0
        self.average_slider1_position = 0
        self.average_slider2_position = 0
        self.average_slider3_position = 0
        self.average_yaw = 0
        self.average_pitch = 0
        self.average_roll = 0
        self.average_x = 0
        self.average_y = 0

        self.locations_visited = np.zeros((int(self._performance.xLength), int(self._performance.yLength)), dtype=np.int)

        self.remaining_energy = 0
        self.get_logger().info("Reset done.")

    def observe_control_inputs(self) -> ControlInput:
        controlInput = ControlInput()

        # get last wheel targets
        start_time=time.time()
        controlInput.Wheel1aTargetSpeed = self._wheels_targets.target_wheel1a_rad
        controlInput.Wheel2aTargetSpeed = self._wheels_targets.target_wheel2a_rad
        controlInput.Wheel3aTargetSpeed = self._wheels_targets.target_wheel3a_rad
        controlInput.Wheel1bTargetSpeed = self._wheels_targets.target_wheel1b_rad
        controlInput.Wheel2bTargetSpeed = self._wheels_targets.target_wheel2b_rad
        controlInput.Wheel3bTargetSpeed = self._wheels_targets.target_wheel3b_rad
        self.get_logger().info("CTRL INPUT wheels: "+str(time.time()-start_time))

        # get last slider targets
        start_time=time.time()
        controlInput.Slider1TargetSpeed = self._slider_targets.target_slider1_mm_s
        controlInput.Slider2TargetSpeed = self._slider_targets.target_slider2_mm_s
        controlInput.Slider3TargetSpeed = self._slider_targets.target_slider3_mm_s

        # get last slider position
        controlInput.Slider1Position = self._slider_pos[0]
        controlInput.Slider2Position = self._slider_pos[1]
        controlInput.Slider3Position = self._slider_pos[2]
        self.get_logger().info("CTRL INPUT sliders: "+str(time.time()-start_time))

        # get last pozyx variables
        start_time=time.time()
        controlInput.XPos = self._pozyx_position[0] / 1000.0
        controlInput.YPos = self._pozyx_position[1] / 1000.0
        controlInput.ZPos = self._pozyx_position[2] / 1000.0
        controlInput.RollAngle  = self._pozyx_angles_rpy[0]
        controlInput.PitchAngle = self._pozyx_angles_rpy[1]
        controlInput.YawAngle   = self._pozyx_angles_rpy[2]

        controlInput.XSpeed = 0.0
        controlInput.YSpeed = 0.0
        controlInput.ZSpeed = 0.0
        controlInput.YawSpeed   = 0.0 #angular_velocity.x
        controlInput.PitchSpeed = 0.0 #angular_velocity.y
        controlInput.RollSpeed  = 0.0 #angular_velocity.z

        controlInput.XAccel = 0.0 #linear_acceleration.x / 1000
        controlInput.YAccel = 0.0 #linear_acceleration.y / 1000
        controlInput.ZAccel = 0.0 #linear_acceleration.z / 1000
        controlInput.YawAccel   = 0.0
        controlInput.PitchAccel = 0.0
        controlInput.RollAccel  = 0.0

        # get last sonar info
        controlInput.ObstacleDist_1a2a3a = 0.0
        controlInput.ObstacleDist_1a2b3a = 0.0
        controlInput.ObstacleDist_1a2b3b = 0.0
        controlInput.ObstacleDist_1a2a3b = 0.0
        controlInput.ObstacleDist_1b2a3a = 0.0
        controlInput.ObstacleDist_1b2b3a = 0.0
        controlInput.ObstacleDist_1b2b3b = 0.0
        controlInput.ObstacleDist_1b2a3b = 0.0
        controlInput.ObstacleDanger_1a2a3a = 0.0
        controlInput.ObstacleDanger_1a2b3a = 0.0
        controlInput.ObstacleDanger_1a2b3b = 0.0
        controlInput.ObstacleDanger_1a2a3b = 0.0
        controlInput.ObstacleDanger_1b2a3a = 0.0
        controlInput.ObstacleDanger_1b2b3a = 0.0
        controlInput.ObstacleDanger_1b2b3b = 0.0
        controlInput.ObstacleDanger_1b2a3b = 0.0

        # safety module check
        controlInput.SpatialDangerXMin = self._safety_module.check_xMin_bounds(controlInput.XPos, controlInput.YPos)
        controlInput.SpatialDangerXMax = self._safety_module.check_xMax_bounds(controlInput.XPos)
        controlInput.SpatialDangerYMin = self._safety_module.check_yMin_bounds(controlInput.XPos, controlInput.YPos)
        controlInput.SpatialDangerYMax = self._safety_module.check_yMax_bounds(controlInput.YPos)
        self.get_logger().info("CTRL INPUT pozyx: "+str(time.time()-start_time))

        # remove oldest stored input
        start_time=time.time()
        removed_input = ControlInput()
        if (len(self._experience_memory) > 1000) :
            removed_input = self._experience_memory.popleft()
        self._experience_memory.append(controlInput)
        self._update_averages(removed_input, controlInput, len(self._experience_memory))
        self.get_logger().info("CTRL INPUT final bit: "+str(time.time()-start_time))
        
        return controlInput

    def apply_control_outputs(self, control_output: ControlOutput, time_step: float):
        #safeControlOutput = self._safety_module.sanitize_output(controlInput, controlOutput)
        safeControlOutput = control_output

        msg = WheelTargetsMsg()
        msg.target_wheel1a_rad = safeControlOutput.Wheel1aTargetSpeed
        msg.target_wheel1b_rad = safeControlOutput.Wheel1bTargetSpeed
        msg.target_wheel2a_rad = safeControlOutput.Wheel2aTargetSpeed
        msg.target_wheel2b_rad = safeControlOutput.Wheel2bTargetSpeed
        msg.target_wheel3a_rad = safeControlOutput.Wheel3aTargetSpeed
        msg.target_wheel3b_rad = safeControlOutput.Wheel3bTargetSpeed
        self._wheels_target_pub.publish(msg)
        self._wheels_targets = msg

        msg = SliderTargetsMsg()
        msg.target_slider1_mm_s = safeControlOutput.Slider1TargetSpeed
        msg.target_slider2_mm_s = safeControlOutput.Slider2TargetSpeed
        msg.target_slider3_mm_s = safeControlOutput.Slider3TargetSpeed
        self._sliders_target_pub.publish(msg)
        self._slider_targets = msg

    def get_status(self, elapsed_time: float)-> (float, bool):
        # return self._get_slider_reward(elapsed_time)[0],False
        # return self._get_location_reward(elapsed_time)
        # return self._get_energy_management_reward(elapsed_time)

        #return self._get_geometric_reward(elapsed_time)
        reward = self._get_slider_reward(elapsed_time)[0] + self._get_location_reward(elapsed_time)[0] + self._get_angular_reward(elapsed_time)[0] + self._get_energy_management_reward(elapsed_time)[0], False
        
        self.get_logger().info("Total reward: "+str(reward))
        
        return reward
        # return (4 * self._get_slider_reward_simple(elapsed_time)[0]) + self._get_angular_reward(elapsed_time)[0] + self._get_energy_conservation(elapsed_time)[0], False
        # return self._get_slider_reward_simple(elapsed_time)[0] + self._get_angular_reward(elapsed_time)[0] + self._get_energy_conservation(elapsed_time)[0], False
        
        # return self._get_circular_reward(elapsed_time)
        # return self._get_slider_reward(elapsed_time)
        # return self._get_angular_reward(elapsed_time)[0] + self._get_energy_conservation(elapsed_time)[0], False
        # return self._get_angular_reward(elapsed_time)
        # return self._get_slider_reward_simple(elapsed_time)
        # reward = 0.0*self._get_circular_reward(elapsed_time)[0] + 1.0*self._get_slider_reward(elapsed_time)[0] + 0.0*self._get_angular_reward(elapsed_time)[0]
        # return reward, self._get_circular_reward(elapsed_time)[1]
        # return reward, False

    def finalize(self):
        """Finalize function, without serial ports closing, which is handled when closing nodes"""
        # set wheel target speed to zero
        msg = WheelTargetsMsg()
        msg.target_wheel1a_rad = 0.0
        msg.target_wheel1b_rad = 0.0
        msg.target_wheel2a_rad = 0.0
        msg.target_wheel2b_rad = 0.0
        msg.target_wheel3a_rad = 0.0
        msg.target_wheel3b_rad = 0.0
        self._wheels_target_pub.publish(msg)
        self._wheels_targets = msg

        # set slider target speed to zero
        msg = SliderTargetsMsg()
        msg.target_slider1_mm_s = 0.0
        msg.target_slider2_mm_s = 0.0
        msg.target_slider3_mm_s = 0.0
        self._sliders_target_pub.publish(msg)
        self._slider_targets = msg

class Coordinate:

    def __init__(self, x, y):
        self._x = x
        self._y = y

    @property
    def x(self):
        return self._x
    
    @x.setter
    def x(self, value):
        self._x = value

    @property
    def y(self):
        return self._y
    
    @y.setter
    def y(self, value):
        self._y = value

    def get_squared(self):
        return self._x**2 + self._y**2

    def get_distance(self):
        return self.get_squared()**0.5

def main(args=None):
    rclpy.init(args=args)
    dai_body_node = DaiBodyNode()

    # infinite loop
    rclpy.spin(dai_body_node)

    rclpy.shutdown()

if __name__ == '__main__':
    main()