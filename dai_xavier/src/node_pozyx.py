#!/usr/bin/env python3
"""
    ROS node that publishes pozyx data:
        pozyx_pose  Pose(Point, Quaternion)

    Device: Jetson Xavier
"""

import os, yaml
from numpy import random

import rclpy
from rclpy.node import Node
from ament_index_python.packages import get_package_share_directory

from pypozyx import *

# from geometry_msgs.msg import Point, Quaternion
# from dai_main.msg import EulerAngles as EulerAnglessMsg
from dai_main.msg import PozyxData as PozyxDataMsg

# false is env var 'ROS_DUMMY' is unset or empty, true otherwise. Publishes fake data.
DUMMY = True if 'ROS_DUMMY' in os.environ and os.environ['ROS_DUMMY'] else False

# anchor coordniates ids
anchor_ids = [0x610a, 0x6110, 0x611c, 0x611a]

class PosePublisher(Node):
    def __init__(self):
        super().__init__('pose_publisher')
        self.init_success = False
        self.pozyx_ = None

        # get performences config
        yaml_config = os.path.join(
            get_package_share_directory('dai_main'),
            'config',
            'performances.yaml'
        )

        # read yaml config
        with open(yaml_config) as config_file :
            config = yaml.load(config_file, Loader=yaml.FullLoader)
        perf_name = config['PerformanceToUse']
        anchors_coords = config[perf_name]['coordinates']

        # setup anchors list
        self.anchors_ = [DeviceCoordinates(device_id,1,Coordinates(coords[0],coords[1],coords[2]))
                         for device_id, coords in zip(anchor_ids, anchors_coords)]

        # pozyx init
        if not DUMMY:
            try:
                self.initialisePozyx()
            except:
                self.destroy_node()
                return

        # create publisher
        self.publisher_ = self.create_publisher(PozyxDataMsg, '/sensors/pozyx_pose', 10)
        
        if not DUMMY: self.timer = self.create_timer(0.1, self.update_pose)
        else: self.timer = self.create_timer(0.1, self.dummy_update_pose)

        self.init_success = True
        self.get_logger().info("INIT_SUCCESS") # for user command service


    def update_pose(self):

        coords = Coordinates()
        quat = Quaternion()
        angles = EulerAngles()

        self.pozyx_.doPositioning(coords)
        self.pozyx_.getQuaternion(quat)
        self.pozyx_.getEulerAngles_deg(angles)

        # publish message
        msg = PozyxDataMsg()
        msg.position.x = coords.x
        msg.position.y = coords.y
        msg.position.z = coords.z
        msg.orientation.x = quat.x
        msg.orientation.y = quat.y
        msg.orientation.z = quat.z
        msg.orientation.w = quat.w
        msg.angles.pitch = angles.pitch
        msg.angles.roll  = angles.roll
        msg.angles.yaw   = angles.heading
        self.publisher_.publish(msg)

    
    def initialisePozyx(self): 

        serial_port = get_first_pozyx_serial_port()
        try:
            self.pozyx_ = PozyxSerial(serial_port)
        except:
            self.get_logger().error("Pozyx: Couldn't connect to port "+str(serial_port))
            raise
        self.get_logger().info("Pozyx: serial_port: "+str(serial_port))

        status = self.pozyx_.clearDevices()
        self.get_logger().info("Pozyx: Anchors cleared: "+str(status))

        self.setAnchorsManual()

        self.pozyx_.setPositionAlgorithmNormal() # setPositionAlgorithmTracking()
        self.pozyx_.setPositioningFilterMovingAverage(8)
        self.pozyx_.setRangingProtocolPrecision() # setRangingProtocolFast()

        # self.pozyx_.setUpdateInterval(100)
                
        self.get_logger().info("Pozyx: Done initializing.")

    def setAnchorsManual(self):
        """Adds the manually measured anchors to the Pozyx's device list one for one."""
        status = self.pozyx_.clearDevices(None)
        for anchor in self.anchors_:
            status &= self.pozyx_.addDevice(anchor, None)
        if len(self.anchors_) > 4:
            status &= self.pozyx_.setSelectionOfAnchors(POZYX_ANCHOR_SEL_AUTO, len(self.anchors_))
        self.get_logger().info("Pozyx: status:"+str(status))
        return status


    def dummy_update_pose(self):
        msg = PozyxDataMsg()
        msg.position.x = random.rand()*100
        msg.position.y = random.rand()*100
        msg.position.z = random.rand()*100
        msg.orientation.x = random.rand()
        msg.orientation.y = random.rand()
        msg.orientation.z = random.rand()
        msg.orientation.w = random.rand()
        msg.angles.pitch = random.rand()*180
        msg.angles.roll  = random.rand()*180
        msg.angles.yaw   = random.rand()*180
        self.publisher_.publish(msg)
        self.get_logger().info("Pozyx: dummy msg published")


def main(args=None):
    rclpy.init(args=args)
    pose_publisher = PosePublisher()

    # infinite loop
    if pose_publisher.init_success :
        rclpy.spin(pose_publisher)

        # while rclpy.ok():
        #     if not DUMMY : pose_publisher.update_pose()
        #     else: 
        #         pose_publisher.dummy_update_pose()
        #         sleep(1)

    rclpy.shutdown()

if __name__ == '__main__':
    main()