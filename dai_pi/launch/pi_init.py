"""
    Launches all Raspberry Pi nodes: wheels, sliders (and sonars)
"""
import socket
from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():
    return LaunchDescription([

        # wheels ---------------------------- #
        Node(
            package='dai_pi',
            node_namespace='/'+socket.gethostname(),
            node_executable='node_wheels.py',
            remappings=[
                ('/'+socket.gethostname()+'/rosout', '/rosout'),
                ('/'+socket.gethostname()+'/parameter_events', '/parameter_events')
            ]
        ),

        # sliders --------------------------- #
        Node(
            package='dai_pi',
            node_namespace='/'+socket.gethostname(),
            node_executable='node_sliders.py',
            remappings=[
                ('/'+socket.gethostname()+'/rosout', '/rosout'),
                ('/'+socket.gethostname()+'/parameter_events', '/parameter_events')
            ]
        ),

        # battery level --------------------- #
        Node(
            package='dai_pi',
            node_namespace='/'+socket.gethostname(),
            node_executable='node_battery.py',
            remappings=[
                ('/'+socket.gethostname()+'/rosout', '/rosout'),
                ('/'+socket.gethostname()+'/parameter_events', '/parameter_events')
            ]
        ),

    ])
