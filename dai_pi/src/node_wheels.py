#!/usr/bin/env python3
"""
    ROS node that listen for wheel speed command and send serial command

    Device: Raspberry Pi
"""

import os, yaml
import math
import serial
import socket

import rclpy
from rclpy.node import Node
from ament_index_python.packages import get_package_share_directory

from dai_main.msg import WheelTargets as WheelTargetsMsg

# false is env var 'ROS_DUMMY' is unset or empty, true otherwise. Publishes fake data.
DUMMY = True if 'ROS_DUMMY' in os.environ and os.environ['ROS_DUMMY'] else False

class WheelSubscriber(Node):
    def __init__(self):
        super().__init__('wheel_subscriber')
        self.init_success = False
        self._ser = None
        self._wheel_ids = []
        self._prev_speed = {}

        # get ip config
        ip_config = os.path.join(get_package_share_directory('dai_main'),'config','ip_config.yaml')
        with open(ip_config) as config_file:
            config = yaml.load(config_file, Loader=yaml.FullLoader)
        device_name = socket.gethostname()
        try: 
            self._wheel_ids = config['raspberrypi_list'][device_name]['wheel_ids']
        except:
            self.get_logger().error('Device <'+device_name+'> not listed in '+ip_config)
            self.destroy_node()
            return

        # stop if there is no wheel to control
        if not self._wheel_ids : 
            self.get_logger().info("No wheel to control on <"+device_name+">. Destroying node.")
            self.destroy_node()
            return

        # get motors config
        motors_config = os.path.join(get_package_share_directory('dai_main'),'config','wheel_config.yaml')
        with open(motors_config) as config_file:
            config = yaml.load(config_file, Loader=yaml.FullLoader)
        self._motor_stall_torque = config['motor_stall_torque']
        self._motor_free_current = config['motor_free_current']
        self._motor_stall_current = config['motor_stall_current']
        self._max_rpm = config['max_rpm']
        self._max_speed = self._max_rpm * (math.pi/30) # max speed in radians per second
        self._motor_ids = config['motor_ids']
        self._serial_port = config['serial_port']
        self._serial_baudrate = config['serial_baudrate']
        self._serial_timeout = config['serial_timeout']

        # connect to serial port
        if not DUMMY:
            try:      
                self._ser = serial.Serial(port=self._serial_port, 
                                          baudrate=self._serial_baudrate)

                if self._ser.is_open:
                    self.get_logger().info("Wheels serial port "+self._serial_port+" ("+str(self._serial_baudrate)+") on "+device_name+" opened.")
                else:
                    self.get_logger().error("Could not open Wheels serial port "+self._serial_port+" ("+str(self._serial_baudrate)+") on "+device_name)

            except Exception as e:
                self.get_logger().error(str(e))
                self.destroy_node()
                return

        # init previous speed
        for wheel_id in self._wheel_ids:
            self._prev_speed[wheel_id] = 0.0

        # create subscriber
        self.subscription = self.create_subscription(
            WheelTargetsMsg,
            '/targets/wheels',
            self.set_speed,
            10)
        self.subscription

        self.init_success = True
        self.get_logger().info("Wheels "+str(self._wheel_ids)+" initialized.")
        self.get_logger().info("INIT_SUCCESS") # for user command service

    def __del__(self): 
        self.closeSerial()

    def set_speed(self, msg):

        for wheel_id in self._wheel_ids :

            target_key = "target_wheel"+wheel_id+"_rad" # must correspond to msg keys
            target_speed = getattr(msg, target_key)

            # don't update speed if it is unchanged
            # if target_speed == self._prev_speed[wheel_id]:
            #     self.get_logger().info("Wheel "+wheel_id+" : speed unchanged ("+str(target_speed)+")")
            #     continue

            if DUMMY: 
                self.get_logger().info("Wheel "+wheel_id+" : DUMMY speed set to "+str(target_speed))
            else:
                if -self._max_speed <= target_speed <= self._max_speed:
                    target_speed_serial = str(int(1500 * (target_speed / self._max_speed))) #1500 is the speed at which the sabertooth sends 18v
                    speed_command_serial = "M" + str(self._motor_ids[wheel_id]) + ":" + target_speed_serial + "\r\n"

                    self.get_logger().info("Wheel "+wheel_id+" : serial command: "+speed_command_serial)
                    
                    self._ser.write(speed_command_serial.encode())

                    self._prev_speed[wheel_id] = target_speed
                    self.get_logger().info("Wheel "+wheel_id+" : speed set to "+str(target_speed))
                else:
                    self.get_logger().info("Wheel "+wheel_id+" : speed too high ! ("+str(target_speed)+")")

    def closeSerial(self):
        if self._ser is not None: 
            self._ser.close()
            self.get_logger().info("Serial connexion closed.")


def main(args=None):
    rclpy.init(args=args)

    wheel_subscriber = WheelSubscriber()

    if wheel_subscriber.init_success:
        while rclpy.ok():
            rclpy.spin_once(wheel_subscriber)    # infinite loop

    rclpy.shutdown()

if __name__ == '__main__':
    main()
