#!/usr/bin/env python3
"""
    ROS node that listen for sliders speed command and send serial command

    Sliders 1,2 on Sabertooth SER=16003F5D76EA
    Slider 3 on Sabertooth SER=16006D4CE9D9

    Device: Raspberry Pi
"""

import os, yaml
import serial
import socket
import time

from numpy import random

import rclpy
from rclpy.node import Node
from ament_index_python.packages import get_package_share_directory

from dai_main.msg import SliderTargets as SliderTargetsMsg
from dai_main.msg import SliderPosition as SliderPositionMsg

# false is env var 'ROS_DUMMY' is unset or empty, true otherwise. Publishes fake data.
DUMMY = True if 'ROS_DUMMY' in os.environ and os.environ['ROS_DUMMY'] else False

class SliderNode(Node):
    def __init__(self):
        super().__init__('slider_node')
        self.init_success = False
        self._ser = None
        self._slider_ids = []

        self._prev_speed = {}

        # get ip config
        ip_config = os.path.join(get_package_share_directory('dai_main'),'config','ip_config.yaml')
        with open(ip_config) as config_file:
            config = yaml.load(config_file, Loader=yaml.FullLoader)
        device_name = socket.gethostname()
        try:
            self._slider_ids = config['raspberrypi_list'][device_name]['slider_ids']
        except:
            self.get_logger().error('Device <'+device_name+'> not listed in '+ip_config)
            self.destroy_node()
            return

        # stop if there is no slider to control
        if not self._slider_ids :
            self.get_logger().info("No slider to control on <"+device_name+">. Destroying node.")
            self.destroy_node()
            return

        # get slider config
        motors_config = os.path.join(get_package_share_directory('dai_main'),'config','slider_config.yaml')
        with open(motors_config) as config_file:
            config = yaml.load(config_file, Loader=yaml.FullLoader)
        self._min_position = config['slider_min_pos']
        self._max_position = config['slider_max_pos']
        self._pub_freq = config['pub_freq']
        self._serial_port = config['serial_port']
        self._serial_baudrate = config['serial_baudrate']
        self._serial_timeout = config['serial_timeout']

        if not DUMMY:
            # connect to serial port
            try:
                self._ser = serial.Serial(port=self._serial_port, 
                                          baudrate=self._serial_baudrate)

                if self._ser.is_open:
                    self.get_logger().info("Slider serial port "+self._serial_port+" ("+str(self._serial_baudrate)+") on "+device_name+" opened.")
                else:
                    self.get_logger().error("Could not open Slider serial port "+self._serial_port+" ("+str(self._serial_baudrate)+") on "+device_name)

            except Exception as e:
                self.get_logger().error(str(e))
                self.destroy_node()
                return

            # initialize slider position
            for slider_id in self._slider_ids:
                self._prev_speed[slider_id] = 0.0   # initialize previous speed

                start_cmd = slider_id+",start" + "\r\n"
                self._ser.write(start_cmd.encode())

                home_cmd  = slider_id+",home" + "\r\n"
                self._ser.write(home_cmd.encode())

                time.sleep(5)
                self.get_logger().info("Slider "+slider_id+" at home position.")

        # create subscriber, waiting to set speed
        self.subscription = self.create_subscription(
            SliderTargetsMsg,
            '/targets/sliders',
            self.set_speed,
            10)
        self.subscription

        # create publishers, sending slider position
        self.publisher_list = {}
        for slider_id in self._slider_ids :
            self.publisher_list[slider_id] = self.create_publisher(SliderPositionMsg, '/sensors/slider'+slider_id+'_pos', 10)
        
        # timer callback to publish slider position
        if not DUMMY: self.timer = self.create_timer(1./self._pub_freq, self.update_slider_pos)
        else: self.timer = self.create_timer(1./self._pub_freq, self.dummy_update_slider_pos)

        self.init_success = True
        self.get_logger().info("Sliders "+str(self._slider_ids)+" initialized.")
        self.get_logger().info("INIT_SUCCESS") # for user command service

    def __del__(self): 
        self.closeSerial()

    def set_speed(self, msg):
        for slider_id in self._slider_ids :

            target_key = "target_slider"+slider_id+"_mm_s"   # must correspond to msg keys
            target_speed = getattr(msg, target_key)

            # don't update speed if it is unchanged
            if target_speed == self._prev_speed[slider_id]:
                self.get_logger().info("Slider "+slider_id+" : speed unchanged ("+str(target_speed)+")")
                continue

            if DUMMY:
                self._prev_speed[slider_id] = target_speed
                self.get_logger().info("Slider "+slider_id+" : DUMMY speed set to "+str(target_speed))
            else :
                kangaroo_speed = target_speed * 1
                slider_cmd = slider_id + ",s" + str(int(kangaroo_speed)) + "\r\n"

                try:
                    self._ser.write(slider_cmd.encode())
                    self._prev_speed[slider_id] = target_speed
                    self.get_logger().info("Slider "+slider_id+" : target speed command : "+slider_cmd)
                except:
                    self.get_logger().error("Slider "+slider_id+" : set_target_speed failed.")

                self.get_logger().info("Slider "+slider_id+" : target speed :"+str(kangaroo_speed)+" "+slider_cmd)

    def update_slider_pos(self):

        for slider_id in self._slider_ids :

            slider_position = None
            cmd = slider_id + ",getp" + "\r\n"
            position_list = []

            try:
                # send serial command
                self._ser.write(cmd.encode())

                # wait for answer
                # read_length, x = 0, 0
                # while read_length <= 0 or x < 100:
                #     read_length = self._ser.inWaiting()  # get nb of bytes available to be read
                #     x += 1
                
                # if x == 100: self.get_logger().warn("Slider "+slider_id+" : Timeout")
                # self.get_logger().info("Slider "+slider_id+" read length: "+str(read_length))

                # if we got an answer
                # if read_length > 0:
                #     read_data = self._ser.read(read_length)  # use .readline() instead ?
                #     read_data = read_data.decode()
                # else : 
                #     self.get_logger().error("Slider "+slider_id+" : No reponse from kangaroo.")
            
                read_data = self._ser.readline() # works like a charm :)
                read_data = read_data.decode()

                if len(read_data) > 0:
                    # self.get_logger().info("Slider "+slider_id+" : Received data: "+read_data)

                    # parse position from serial data (only considers first par of the received message)
                    # data format: <ID>,P<POS>
                    parsed_data = read_data.split(",P",1)
                    if parsed_data[0] == slider_id:
                        slider_position = int(float(parsed_data[1])) - 300
                        # self.get_logger().info("Slider "+slider_id+" : Received position: "+parsed_data[1]+" -> "+str(slider_position))
                    # if we get the position of another slider, update its position instead.
                    elif parsed_data[0] in self._slider_ids:
                        slider_position = int(float(parsed_data[1])) - 300
                        self.get_logger().info("Slider "+slider_id+" : Received position from Slider "+parsed_data[0])
                        slider_id = parsed_data[0]
                else:
                    self.get_logger().warn("Slider "+slider_id+" : Serial read empty.")


            except:
                self.get_logger().error("Slider "+slider_id+" : Error retrieving slider position.")
                continue # continue with next slider

            if slider_position is None:
                continue # continue with next slider

            # cap values beyond stop limit (see config file)
            slider_position = max(min(slider_position, self._max_position), self._min_position)

            # publish data
            msg = SliderPositionMsg()
            msg.slider_id = slider_id
            msg.slider_position = slider_position
            self.publisher_list[slider_id].publish(msg)

    def closeSerial(self):
        if self._ser is not None: 
            self._ser.close()
            self.get_logger().info("Serial connexion closed.")

    def dummy_update_slider_pos(self):
        for slider_id in self._slider_ids :
            msg = SliderPositionMsg()
            msg.slider_id = slider_id
            msg.slider_position = random.randint(50)
            self.publisher_list[slider_id].publish(msg)


def main(args=None):
    rclpy.init(args=args)

    slider_node = SliderNode()
    if slider_node.init_success:
        # while rclpy.ok():
        rclpy.spin(slider_node)    # infinite loop

    rclpy.shutdown()

if __name__ == '__main__':
    main()
