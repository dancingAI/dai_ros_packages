#!/usr/bin/env python3
"""
    ROS node that publishes the battery level,
        using serial port of a slider.

    Device: Raspberry Pi
"""

import os, yaml
from numpy import random
import serial
import socket

import rclpy
from rclpy.node import Node
from ament_index_python.packages import get_package_share_directory

from std_msgs.msg import Float32

class BatteryPublisher(Node):
    def __init__(self):
        super().__init__('battery_publisher')
        self.init_success = False
        self._ser = None

        # get slider config (battery read is using the same serial config, since it's reading from the same port)
        motors_config = os.path.join(get_package_share_directory('dai_main'),'config','battery_config.yaml')
        with open(motors_config) as config_file:
            config = yaml.load(config_file, Loader=yaml.FullLoader)
        self._pub_freq = config['pub_freq']
        self._serial_port = config['serial_port']
        self._device_reading = config['device_reading']
        self._motor_id = config['motor_id']
        self._serial_baudrate = config['serial_baudrate']
        self._serial_timeout = config['serial_timeout']
        self._warning_level = config['warning_level']

        # find out if this node should run on this device
        device_name = socket.gethostname()
        if self._device_reading == device_name:
            self.get_logger().info('Battery reading enabled on <'+device_name+'>. Warning level: '+str(self._warning_level)+' V')
        else:
            self.get_logger().info('Battery read not configured for <'+device_name+'>')
            self.destroy_node()
            return

        # connect to serial port
        try:
            self._ser = serial.Serial(port=self._serial_port, 
                                      baudrate=self._serial_baudrate, 
                                      timeout=self._serial_timeout)

            if self._ser.is_open:
                self.get_logger().info("Battery serial port "+self._serial_port+" ("+str(self._serial_baudrate)+") on "+device_name+" opened.")
            else:
                self.get_logger().error("Could not open Battery serial port "+self._serial_port+" ("+str(self._serial_baudrate)+") on "+device_name)

        except Exception as e:
            self.get_logger().error(str(e))
            self.destroy_node()
            return

        # create publisher
        self.publisher_ = self.create_publisher(Float32, '/sensors/battery', 10)
        self.timer = self.create_timer(1./self._pub_freq, self.update_battery)
        

        self.init_success = True
        self.get_logger().info("INIT_SUCCESS") # for user command service


    def update_battery(self):

        cmd = "M"+self._motor_id+":getb\r\n"
        battery_level = None

        self._ser.write(cmd.encode())

        read_data = self._ser.readline()
        read_data = read_data.decode()

        # parse received data
        if len(read_data) > 0:
            try:
                parsed_data = read_data.split(":B",1)
                battery_level = int(parsed_data[1])/10.
            except:
                self.get_logger().warn("Battery: Error reading data: "+read_data)
                return
        else:
            self.get_logger().warn("Battery: Serial read empty.")
            return

        # print warning if battery is low
        if battery_level is not None:
            
            if battery_level <= self._warning_level:
                self.get_logger().warn("!!! Low Battery ("+str(battery_level)+" V) !!! ")

            # publish message
            msg = Float32()
            msg.data = battery_level
            self.publisher_.publish(msg)

    def closeSerial(self):
        if self._ser is not None: 
            self._ser.close()
            self.get_logger().info("Serial connexion closed.")

    def __del__(self): 
        self.closeSerial()


def main(args=None):
    rclpy.init(args=args)
    battery_publisher = BatteryPublisher()

    # infinite loop
    if battery_publisher.init_success :
        rclpy.spin(battery_publisher)

    rclpy.shutdown()

if __name__ == '__main__':
    main()