#!/usr/bin/env python3
"""
    ROS node that publishes sonar data (todo)

    Device: Raspberry Pi
"""

import os, yaml
import serial
import socket

import rclpy
from rclpy.node import Node
from ament_index_python.packages import get_package_share_directory

class SonarPublisher(Node):
    def __init__(self):
        super().__init__('sonar_publisher')


        # create publisher
        self.publisher_ = self.create_publisher(Pose, 'sonar', 10)

    def update_sonar(self):
        pass


    def __del__(self): 
        self.closeSerial()


    def closeSerial(self):
        self._ser.close()

def main(args=None):
    rclpy.init(args=args)

    sonar_publisher = SonarPublisher()
    # infinite loop
    while rclpy.ok():
        sonar_publisher.update_sonar()

    rclpy.shutdown()

if __name__ == '__main__':
    main()