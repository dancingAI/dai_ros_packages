#!/usr/bin/env python3
# 
# User command Client. Run a single device (any)
# 

# https://github.com/RoverRobotics/ros2-keyboard-driver/blob/master/keystroke/listen.py

import os, sys

from dai_main.srv import Pi2aUserCommand, Pi2bUserCommand, Pi3aUserCommand
from dai_main.srv import Nano2aUserCommand, Nano2bUserCommand, Nano3aUserCommand
from dai_main.srv import XavierUserCommand
import rclpy
from rclpy.node import Node

help_str = ("Available commands:\n"
            "  - init   <wheels> <sliders> <pozyx> <detectnet> <battery> <all>\n"
            # "  - close  <wheels> <sliders> <pozyx> <detectnet> <battery> <all>\n"
            "  - run\n"
            "  - stop")

class UserCommandClient(Node):

    def __init__(self):
        super().__init__('user_command_client')

        # use a different service client for each device
        # ROS2 doesn't allow having multiple instances of a service at the same time

        self.COMMAND_ALLOWED = False
        self._client_table = {}
        self._request_table = {}
        self.pid_table = {}
        self.future_table = {}

        # the env variable must be set up inside the docker image
        self.main_control_dir = os.getenv("DAI_MAIN_CONTROL_DIR")

        self._client_table['Xavier'] = self.create_client(XavierUserCommand, 'Xavier/user_command')
        while not self._client_table['Xavier'].wait_for_service(timeout_sec=1.0):
            self.get_logger().info('[1/7] Xavier service not available, waiting again...')
        self._request_table['Xavier'] = XavierUserCommand.Request()

        self._client_table['Pi2a'] = self.create_client(Pi2aUserCommand, 'Pi2a/user_command')
        while not self._client_table['Pi2a'].wait_for_service(timeout_sec=1.0):
            self.get_logger().info('[2/7] Pi2a service not available, waiting again...')
        self._request_table['Pi2a'] = Pi2aUserCommand.Request()

        self._client_table['Pi2b'] = self.create_client(Pi2bUserCommand, 'Pi2b/user_command')
        while not self._client_table['Pi2b'].wait_for_service(timeout_sec=1.0):
            self.get_logger().info('[3/7] Pi2b service not available, waiting again...')
        self._request_table['Pi2b'] = Pi2bUserCommand.Request()

        self._client_table['Pi3a'] = self.create_client(Pi3aUserCommand, 'Pi3a/user_command')
        while not self._client_table['Pi3a'].wait_for_service(timeout_sec=1.0):
            self.get_logger().info('[4/7] Pi3a service not available, waiting again...')
        self._request_table['Pi3a'] = Pi3aUserCommand.Request()

        self._client_table['Nano2a'] = self.create_client(Nano2aUserCommand, 'Nano2a/user_command')
        while not self._client_table['Nano2a'].wait_for_service(timeout_sec=1.0):
            self.get_logger().info('[5/7] Nano2a service not available, waiting again...')
        self._request_table['Nano2a'] = Nano2aUserCommand.Request()

        self._client_table['Nano2b'] = self.create_client(Nano2bUserCommand, 'Nano2b/user_command')
        while not self._client_table['Nano2b'].wait_for_service(timeout_sec=1.0):
            self.get_logger().info('6/7] Nano2b service not available, waiting again...')
        self._request_table['Nano2b'] = Nano2bUserCommand.Request()

        self._client_table['Nano3a'] = self.create_client(Nano3aUserCommand, 'Nano3a/user_command')
        while not self._client_table['Nano3a'].wait_for_service(timeout_sec=1.0):
            self.get_logger().info('[7/7] Nano3a service not available, waiting again...')
        self._request_table['Nano3a'] = Nano3aUserCommand.Request()

        self.get_logger().info("All clients initialized successfully.")
        self.COMMAND_ALLOWED = True

    def send_request(self, device, cmd_type, node, cmd):
        self._request_table[device].command_type = cmd_type
        self._request_table[device].command = cmd
        self.future_table[device,node] = self._client_table[device].call_async(self._request_table[device])

    def listen_for_input(self):
        if self.COMMAND_ALLOWED:
            user_cmd = input("\nUser command >> ")
            self.COMMAND_ALLOWED = False
            self.command_callback(user_cmd)

    def command_callback(self, user_cmd):

        # initialize nodes
        if "init" in user_cmd:
            if "all" in user_cmd or "wheels" in user_cmd:
                cmd = "ros2 run dai_pi node_wheels.py"
                self.send_request("Pi2a", "init", "wheels", cmd)
                self.send_request("Pi2b", "init", "wheels", cmd)
                self.send_request("Pi3a", "init", "wheels", cmd)
                self.get_logger().info('Wheels initialized')
            if "all" in user_cmd or "sliders" in user_cmd:
                cmd = "ros2 run dai_pi node_sliders.py"
                self.send_request("Pi2a", "init", "sliders", cmd)
                self.send_request("Pi2b", "init", "sliders", cmd)
                self.send_request("Pi3a", "init", "sliders", cmd)
                self.get_logger().info('Sliders initialized')
            if "all" in user_cmd or "pozyx" in user_cmd:
                cmd = "ros2 run dai_xavier node_pozyx.py"
                self.send_request("Xavier", "init", "pozyx", cmd)
                self.get_logger().info('Pozyx initialized')
            if "all" in user_cmd or "detectnet" in user_cmd:
                cmd = "ros2 launch dai_nano detectnet.launch"
                self.send_request("Nano2a", "init", "detectnet", cmd)
                self.send_request("Nano2b", "init", "detectnet", cmd)
                self.send_request("Nano3a", "init", "detectnet", cmd)
                self.get_logger().info('Detectnet initialized')
            if "all" in user_cmd or "battery" in user_cmd:
                cmd = "ros2 run dai_pi node_battery.py"
                self.send_request("Pi2a", "init", "battery", cmd)
                self.send_request("Pi2b", "init", "battery", cmd)
                self.send_request("Pi3a", "init", "battery", cmd)
                self.get_logger().info('Battery initialized')
            if user_cmd.strip() == 'init':
                self.get_logger().warn("Error: You must specify which nodes to init.")
                self.get_logger().info(help_str)
            self.COMMAND_ALLOWED = True

        
        # close nodes       NOT WORKING 
        # elif "close" in user_cmd:
        #     if "all" in user_cmd or "wheels" in user_cmd:
        #         self.send_request("Pi2a", "close", "wheels", str(self.pid_table[("Pi2a","wheels")]))
        #         self.send_request("Pi2b", "close", "wheels", str(self.pid_table[("Pi2b","wheels")]))
        #         self.send_request("Pi3a", "close", "wheels", str(self.pid_table[("Pi3a","wheels")]))
        #     if "all" in user_cmd or "sliders" in user_cmd:
        #         self.send_request("Pi2a", "close", "sliders", str(self.pid_table[("Pi2a","sliders")]))
        #         self.send_request("Pi2b", "close", "sliders", str(self.pid_table[("Pi2b","sliders")]))
        #         self.send_request("Pi3a", "close", "sliders", str(self.pid_table[("Pi3a","sliders")]))
        #     if "all" in user_cmd or "pozyx" in user_cmd:
        #         self.send_request("Xavier", "close", "pozyx", str(self.pid_table[("Xavier","pozyx")]))
        #     if "all" in user_cmd or "detectnet" in user_cmd:
        #         self.send_request("Nano2a", "close", "detectnet", str(self.pid_table[("Nano2a","detectnet")]))
        #         self.send_request("Nano2b", "close", "detectnet", str(self.pid_table[("Nano2b","detectnet")]))
        #         self.send_request("Nano3a", "close", "detectnet", str(self.pid_table[("Nano3a","detectnet")]))
        #     if "all" in user_cmd or "battery" in user_cmd:
        #         self.send_request("Pi2a", "close", "battery", str(self.pid_table[("Pi2a","battery")]))
        #         self.send_request("Pi2b", "close", "battery", str(self.pid_table[("Pi2b","battery")]))
        #         self.send_request("Pi3a", "close", "battery", str(self.pid_table[("Pi3a","battery")]))
        #     if user_cmd.strip() == 'close':
        #         self.get_logger().warn("Error: You must specify which nodes to close.")
        #         self.get_logger().info(help_str)
        #         self.COMMAND_ALLOWED = True

        # run Main script
        elif "run" in user_cmd:
            main_script_dir = os.path.join(self.main_control_dir, "src")
            cmd = "cd "+main_script_dir+" && python Main.py"
            self.send_request("Xavier", "run", "main", cmd)
            self.get_logger().info(f'Running {main_script_dir}/Main.py')
            self.COMMAND_ALLOWED = True

        # stop Main script
        elif "stop" in user_cmd:
            self.send_request("Xavier", "stop", "main", str(self.pid_table[("Xavier","main")]))
            self.get_logger().info(f'Stopping Xavier')
            self.COMMAND_ALLOWED = True

        else:
            self.get_logger().warn("Error: unrecognized command")
            self.get_logger().info(help_str)
            self.COMMAND_ALLOWED = True



def main(args=None):
    rclpy.init(args=args)

    user_command_client = UserCommandClient()

    while rclpy.ok():

        # listen for user cmd (if allowed)
        user_command_client.listen_for_input()

        # index = (device, node) tuple
        futures_to_delete = []
        for index, future in user_command_client.future_table.items() :

            # if user cmd is allowed, do not wait for future (avoids getting stuck)
            if user_command_client.COMMAND_ALLOWED : continue

            # spin node
            rclpy.spin_until_future_complete(user_command_client, future)

            if future.done():
                try:
                    response = future.result()
                except Exception as e:
                    user_command_client.get_logger().warn('Service call failed %r' % (e,))
                else:
                    if response.exit_code == "INIT_SUCCESS":
                        user_command_client.pid_table[index] = response.pid
                        user_command_client.get_logger().info("["+index[0]+"] : "+index[1]+" node initialized. PID: "+str(response.pid))
                    elif response.exit_code == "RUN_STARTED":
                        user_command_client.pid_table[index] = response.pid
                        user_command_client.get_logger().info("["+index[0]+"] : "+index[1]+" performance started.")
                    elif response.exit_code == "RUN_STOPPED":
                        del user_command_client.pid_table[index]
                        user_command_client.get_logger().info("["+index[0]+"] : "+index[1]+" performance stopped.")
                    elif response.exit_code == "CLOSE_SUCCESS":
                        del user_command_client.pid_table[index]
                        user_command_client.get_logger().info("["+index[0]+"] : "+index[1]+" node stopped.")
                    else:
                        user_command_client.get_logger().warn("["+index[0]+"] ["+index[1]+"] Error code: "+response.exit_code)

                futures_to_delete.append(index)
                
                # allow user command
                user_command_client.COMMAND_ALLOWED = True

        # delete done futures (outside the previous loop)
        for index in futures_to_delete:
            del user_command_client.future_table[index]

        
    user_command_client.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
