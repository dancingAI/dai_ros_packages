#!/usr/bin/env python3
# 
# User command Service. Run on every device.
# 

# https://www.endpoint.com/blog/2015/01/28/getting-realtime-output-using-python

import os
import signal
import subprocess
import time
import socket

from dai_main.srv import Pi2aUserCommand, Pi2bUserCommand, Pi3aUserCommand
from dai_main.srv import Nano2aUserCommand, Nano2bUserCommand, Nano3aUserCommand
from dai_main.srv import XavierUserCommand
import rclpy
from rclpy.node import Node


class UserCommandService(Node):

    def __init__(self):
        self._device_name = socket.gethostname()
        super().__init__(self._device_name+'_user_command_service')

        # start a different service on each device
        # ROS2 doesn't allow having multiple instances of a service at the same time
        if "Xavier" == self._device_name:
            self.srv = self.create_service(XavierUserCommand, 'Xavier/user_command', self.user_command_callback)
        elif "Pi2a" == self._device_name:
            self.srv = self.create_service(Pi2aUserCommand, 'Pi2a/user_command', self.user_command_callback)
        elif "Pi2b" == self._device_name:
            self.srv = self.create_service(Pi2bUserCommand, 'Pi2b/user_command', self.user_command_callback)
        elif "Pi3a" == self._device_name:
            self.srv = self.create_service(Pi3aUserCommand, 'Pi3a/user_command', self.user_command_callback)
        elif "Nano2a" == self._device_name:
            self.srv = self.create_service(Nano2aUserCommand, 'Nano2a/user_command', self.user_command_callback)
        elif "Nano2b" == self._device_name:
            self.srv = self.create_service(Nano2bUserCommand, 'Nano2b/user_command', self.user_command_callback)
        elif "Nano3a" == self._device_name:
            self.srv = self.create_service(Nano3aUserCommand, 'Nano3a/user_command', self.user_command_callback)
        else:
            self.get_logger().error('Cannot find a Service for device <'+self._device_name+'>')
            self.destroy_node()
            return

        self.get_logger().info("Service started successfully on "+self._device_name)


    def user_command_callback(self, request, response):

        cmd_type = request.command_type

        if "init" in cmd_type:
            response = self.init_command(request.command, response)
        # elif "close" in cmd_type:
        #     response = self.close_command(request.command, response)
        elif "run" in cmd_type:
            response = self.run_command(request.command, response)
        elif "stop" in cmd_type:
            response = self.stop_command(request.command, response)
        else:
            self.get_logger().warn("Unrecognized command type: "+cmd_type)

        return response

    def init_command(self, cmd, response):
        """Initializes harware control nodes."""

        self.get_logger().info("Received command: "+cmd)

        # run command
        try:
            process = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
        except Exception as e:
            self.get_logger().warn("Could not run command.")
            self.get_logger().info(e)

            response.exit_code = "CMD_ERROR"
            return response

        # print stdout while command is running
        # when stdout contains INIT_SUCCESS or on error, break
        while True :
            output = process.stdout.readline()
            if output == '' and process.poll() is not None:
                exit_code = process.poll()
                break
            if output:
                output = output.strip().decode("utf-8") 
                if "INIT_SUCCESS" in output:
                    exit_code = "INIT_SUCCESS"
                    self.get_logger().info('INIT_SUCCESS')
                    break
                else:
                    self.get_logger().info('stdout: '+output)

        response.pid = process.pid
        response.exit_code = exit_code
        return response

    def close_command(self, pid, response):
        """Kills hardware control nodes."""
        os.killpg(os.getpgid(int(pid)), signal.SIGINT)
        os.killpg(os.getpgid(int(pid)), signal.SIGINT)
        # time.sleep(1)
        # os.killpg(os.getpgid(int(pid)), signal.SIGTERM)

        response.pid = 0
        response.exit_code = "CLOSE_SUCCESS"

        return response

    def run_command(self, cmd, response):
        """Starts performance. (Only on Xavier) """

        if self._device_name == "Xavier":
            # run command
            process = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)

            # print stdout while command is running
            # when stdout contains INIT_SUCCESS or on error, break
            while True :
                output = process.stdout.readline()
                if output == '' and process.poll() is not None:
                    exit_code = process.poll()
                    break
                if output:
                    output = output.strip().decode("utf-8") 
                    if "RUN_STARTED" in output:
                        exit_code = "RUN_STARTED"
                        break
                    else:
                        self.get_logger().info('stdout: '+output)

            response.pid = process.pid
            response.exit_code = exit_code
        else:
            response.pid = 0
            response.exit_code = "ERROR"

        return response

    def stop_command(self, pid, response):
        """Stops running performance. [will stop the service too...]"""

        os.killpg(os.getpgid(int(pid)), signal.SIGINT)
        os.killpg(os.getpgid(int(pid)), signal.SIGINT)
        # os.killpg(os.getpgid(int(pid)), signal.SIGTERM)

        response.pid = 0
        response.exit_code = "RUN_STOPPED"

        return response

def main(args=None):
    rclpy.init(args=args)

    user_command_service = UserCommandService()

    rclpy.spin(user_command_service)

    rclpy.shutdown()


if __name__ == '__main__':
    main()