**This repo contains all the files used to build the the ROS package used within Dai.**

It is extensively inspired by dusty-nv [ros_deep_learning package](https://github.com/dusty-nv/ros_deep_learning) (and uses some of its nodes).

This package is designed to be used with ROS2, but still contains a fair amount of ROS code.

**Included in Dai's [eloquent-inference](https://gitlab.com/dancing-ai/dai_docker) docker container.**


- Nodes/Topics graph of the dai_detectnet launch, running in a JetsonNano: ![Ros graph with a single JetsonNano](img/rosgraph_single_JetsonNano.png?raw=true "Nodes/Topics graph with a single JetsonNano with two cameras")